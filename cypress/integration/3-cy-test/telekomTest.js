/// <reference types="cypress" />

describe('test telekom.de', () => {
    beforeEach(() => {
      cy.visit('https://telekom.de/start')
      // id is referred to as #
      cy.get('#consentAcceptAll').should('be.visible').click()
    })

    it('look for the search', () => {
        //cy.get("//button[@data-tealium-rel='header.button.suche']").should('be.visible')
        cy.get('.js-chf-meta-nav_search')
        .should('be.visible')
        cy.get('.js-chf-meta-nav_search > .chf-meta-navigation__trigger').click()
        // how to send things in the field
        cy.get('.nexus-pk-coin__search-bar > input')
        .should('be.visible')
        .clear()
        .type('Smartphone-Tarife')
    })

});