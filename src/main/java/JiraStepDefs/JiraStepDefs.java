package JiraStepDefs;

import Utils.jira_xray.XRayCucumberFeatureFetcher;
import Utils.jira_xray.XRayCucumberResultsImporter;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JiraStepDefs {
    File outputDir;
    File results;

    @Given("feature folder is defined")
    public void featureFolderIsDefined() {
        outputDir = new File("src/test/java/featuresJira");
    }

    @When("feature {string} is exported from Jira Xray")
    public void featureIsExportedFromJiraXray(String id) {
        XRayCucumberFeatureFetcher xRayCucumberFeatureFetcher = new XRayCucumberFeatureFetcher();
        List<String> ids = new ArrayList<String>();
        ids.add(id);
        xRayCucumberFeatureFetcher.fetch(ids, outputDir);
    }

    @Then("feature is successfully imported")
    public void featureIsSuccessfullyImported() {
        File[] files = outputDir.listFiles();
        assert Objects.requireNonNull(files).length != 0;
    }

    @Given("file with results is defined")
    public void fileWithResultsIsDefined() {
        results = new File("target/cucumber-html-reports/cucumber.json");
    }

    @When("results are sent to Jira")
    public void resultsAreSentToJira() {
        XRayCucumberResultsImporter XRayCucumberResultsImporter = new XRayCucumberResultsImporter();
        XRayCucumberResultsImporter.importResults(results);
    }

    @Then("results are sent")
    public void resultsAreSent() {
        outputDir = new File("src/test/java/featuresJira");
        File[] files = outputDir.listFiles();
        assert files != null;
        for (File f: files){
            f.delete();
        }
    }
}
