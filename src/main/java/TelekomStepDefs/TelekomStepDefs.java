package TelekomStepDefs;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import javax.lang.model.element.Element;
import java.util.Locale;
import java.util.Objects;

import static com.codeborne.selenide.Selenide.*;

public class TelekomStepDefs {

    String startPage = "https://www.telekom.de/";
    By acceptance = By.id("consentAcceptAll");
    By mainPageIndicator = By.id("PHX_home_r1");
    By mobilfunk = By.xpath("//a[@aria-label='Mobilfunk']");
    By tariffen = By.xpath("//a[@aria-label='Smartphone-Tarife']");
    By taroffPageIndicator = By.xpath("//h1[contains(text(), 'MagentaMobil: Unsere Smartphone-Tarife')]");
    By buttons = By.xpath("//a[@data-qa='button']");
    By tariffSubHeader = By.xpath("//div[@data-qa='label_TariffSubHeader']");
    By checkbox = By.name("keepExistingNumber");
    By unterwegsIcons = By.xpath("//ul[@class = 'nav']/li");
    By roundIcons = By.xpath("//ul[@class = 'category-nav__list']/li");
    By price = By.xpath("//*[@data-qa = 'label_OTCBar']");

    @Given("start telekom page is open")
    public void startTelekomPageIsOpen() throws InterruptedException {
        open(startPage);
        try{
            $(acceptance).click();
        }
        catch (NoSuchElementException e){
            System.out.println("didn't find the element");
        }
//        Thread.sleep(2000);
        // Addittional check that we can see the page
        $(mainPageIndicator).shouldBe(Condition.visible);
    }


    @When("navigate to tariff cards page")
    public void navigateToTariffCardsPage() {
        $(mobilfunk).hover();
        $(tariffen).click();
        // check page by element
        $(taroffPageIndicator).shouldBe(Condition.visible);
    }

    @And("select first tariff card")
    public void selectFirstTariffCard() {
        $$(buttons).get(0).click();
    }

    @Then("subheader is displayed")
    public void subheaderIsDisplayed() {
        $(tariffSubHeader).shouldBe(Condition.visible);
    }

    @And("checkbox is not checked")
    public void checkboxIsNotChecked() {
        $(checkbox).shouldNotBe(Condition.checked);
    }

    @And("select {string} tariff card")
    public void selectTariffCard(String tariffCardNumber) {
        switch (tariffCardNumber) {
            case "first" -> $$(buttons).get(0).click();
            case "second" -> $$(buttons).get(1).click();
            default -> $$(buttons).get(2).click();
        }
    }

    @When("navigate to {string} menu")
    public void navigateToMenu(String menuName) {
        if ("mobilfunk".equals(menuName.toLowerCase(Locale.ROOT))) {
            $(mobilfunk).click();
        } else {
            $(String.format("//a[@aria-label='%s']", menuName)).click();
        }

    }

    @And("check that url is {string}")
    public void checkTheURL(String url) {
        assert Objects.equals(WebDriverRunner.getWebDriver().getCurrentUrl(), url);
    }

    @And("check there are {int} round icons")
    public void checkTheAmountOfIcons(int amount) {
        ElementsCollection icons = $$(roundIcons);
        assert icons.size() == amount;
    }

    @Then("select {string} icon")
    public void selectIcon(String text) {
        $x(String.format("//li[@class = 'nav__item']//*[contains(text(), '%s')]", text)).click();
    }

    @Then("select {string}")
    public void select(String preciseWording) {
        $x(String.format("//span[contains(text(), '%s')]", preciseWording)).click();
    }

    @And("check the price")
    public void checkThePrice() {
        ElementsCollection spans = $$(price);
        for (com.codeborne.selenide.SelenideElement span : spans) {
            assert span.innerText().contains("229");
        }
    }

    @And("check there are {int} square icons")
    public void checkThereAreSquareIcons(int amount) {
        ElementsCollection icons = $$(unterwegsIcons);
        assert icons.size() == amount;
    }

    @Then("select {string} round icon")
    public void selectRoundIcon(String word) {
        ElementsCollection labels = $$(roundIcons);
        for (com.codeborne.selenide.SelenideElement label : labels) {
            if (label.innerText().contains(word)) {
                label.click();
                break;
            }
        }
    }
}
