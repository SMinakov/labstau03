import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;

public class Lab03Test {

    By pricinglink = By.linkText("Pricing");
    By fourTabs = By.xpath("//*[@class='nav-tabs']/li");
    By promoText = By.xpath("//*[contains(text(), 'Cloud plans')]");
    By threeTabs = By.xpath("//div[@class='owl-stage']/div[@class='owl-item active']");


    @BeforeMethod
    public void init() {
        open("http://gatling.io");
    }

    @Test
    public void openGatlingPage(){
        $(pricinglink).click();
        $(promoText).shouldBe(Condition.visible);
        $$(fourTabs)
                .shouldHave(CollectionCondition.size(4))
                .shouldHave(CollectionCondition.textsInAnyOrder("Cloud", "Self-hosted", "Azure", "Marketplace"));
        $$(threeTabs)
                .shouldHave(CollectionCondition.size(3))
                .shouldHave(CollectionCondition.textsInAnyOrder("Scout", "Scale", "Corporate"));
    }
}
