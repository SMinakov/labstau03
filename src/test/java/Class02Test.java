import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class Class02Test {

    By gatlinglogo = By.className("logo_link");
    By pricinglink = By.xpath("//div[@class='header_main']//a[contains(text(), 'Pricing')]");
    By pricePageSubText = By.xpath("//h1[contains(text(), 'Gatling Enterprise Pricing')]/../p");

// Selenide:
// Does the same as StartPageTest.java but in Selenide
    @Test
    public void gatlingOpen() {
        // headless browser:
        Configuration.headless = false;
        // Open Chrome
        open("http://gatling.io");
        // Make browser a certain size
        Configuration.browserSize="1920x1080";
        // Check that gatlinglogo is visible
        $(gatlinglogo).shouldBe(Condition.visible);
        // Look for a required link and click it
        $(pricinglink)
                .shouldBe(Condition.visible)
                .click();
        // Check text with double check
        $(pricePageSubText)
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text("Choose the best plan for you among all our offers."));
}
}
