import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class StartPageTest {

    WebDriver driver;
    By gatlingLogo = By.className("logo_link");

    @BeforeMethod
    public void init() {
        driver = new ChromeDriver();
        driver.get("http://gatling.io");
    }

    @Test(description = "Opens Gatling Page")
    public void openGatlingPage() throws InterruptedException {
//        Thread.sleep(2000);
        // look for "logo_link" class, link_text = "https://gatling.io/"
        Assert.assertTrue(driver.findElement(gatlingLogo).isDisplayed(), "Element not found");

        List<WebElement> elements = driver.findElements(gatlingLogo);
        Assert.assertFalse(elements.isEmpty(), "No elements with such link");
//        Assert.assertTrue(elements.isEmpty(), "Test checks what we expect it to check");
    }

    @Test(enabled = false)
    public void openPricingPage() throws InterruptedException {
        driver.get("https://gatling.io/enterprise/");

        WebElement pricing = driver.findElement(By.xpath("//div[@class='header_main']//a[contains(text(), 'Pricing')]"));
        Assert.assertTrue(pricing.isDisplayed(), "Pricing was not found");
        pricing.click();

        WebElement header = driver.findElement(By.xpath("//h1[contains(text(), 'Gatling Enterprise Pricing')]"));
        Assert.assertTrue(header.isDisplayed(), "There is no header");

        WebElement paragraph = header.findElement(By.xpath("//h1[contains(text(), 'Gatling Enterprise Pricing')]/../p"));
        Assert.assertEquals(paragraph.getText(), "Choose the best plan for you among all our offers.",
                "Text was not found!");

        SoftAssert softAssert = new SoftAssert();
        // Doesn't drop the test if one of assert conditions aren't met
        softAssert.assertTrue(header.isDisplayed(), "Header is missing");
        softAssert.assertTrue(paragraph.isDisplayed(), "Paragraph is not there");
        softAssert.assertTrue(paragraph.isEnabled(), "Paragraph is not Enabled");
        softAssert.assertAll();
    }

    @Test
    public void loginTest() throws InterruptedException {
//        driver.get("http://gatling.io/");

        WebElement loginButton = driver.findElement(By.xpath("//ul[@id='menu-navbar']//a[@href='https://cloud.gatling.io/#/login']"));
        Assert.assertTrue(loginButton.isDisplayed());
        loginButton.click();
        // Because we don't know WebDriver.Wait yet
        Thread.sleep(2000);

        WebElement userNameField = driver.findElement(By.id("username"));
        userNameField.sendKeys("Randomstring");
        driver.findElement(By.name("login"));
        driver.findElement(By.xpath("//*[@id='kc-login']"));
        driver.findElement(By.id("kc-login"));

        Assert.assertTrue(driver.findElement(By.id("kc-login")).isDisplayed(), "There is no submit button");
    }

    @AfterMethod
    public void quit() {
        driver.quit();
    }
}
