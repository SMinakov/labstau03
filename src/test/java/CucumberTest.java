import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/featuresJira",
        // glue - to ensure the steps are gathered from the right source. Glue is optional.
        glue = "TelekomStepDefs",
        plugin = {
                "pretty",
                "json:target/cucumber-html-reports/cucumber.json"
        },
        publish = true // make sure report is generated
)

public class CucumberTest {
//    telekom.de -> mobilefunk -> Smartfon-tarifen -> whatever
}
