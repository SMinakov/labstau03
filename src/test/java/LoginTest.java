import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class LoginTest {

    @Test
    public void loginTest() throws InterruptedException {
//        System.setProperty("webdriver.chrome.driver", "c:\\JavaThings\\Chrome97\\chromedriver.exe");
//        System.setProperty("webdriver.gecko.driver", "c:\\JavaThings\\geckodriver.exe");
//        System.setProperty("webdriver.edge.driver", "c:\\JavaThings\\msedgedriver.exe");

        WebDriver chrome = new ChromeDriver();
        chrome.get("https://dtag.webex.com/");
        Thread.sleep(5000);
        chrome.quit();
//        Alternative way of closing tab
//        chrome.close();

        WebDriver firefox = new FirefoxDriver();
        firefox.navigate().to("https://dtag.webex.com/");
        Thread.sleep(5000);
        firefox.quit();

//        WebDriver edge = new EdgeDriver();
//        edge.get("https://dtag.webex.com/");
//        Thread.sleep(5000);
//        edge.quit();
    }
}
