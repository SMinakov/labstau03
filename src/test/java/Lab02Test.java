import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Array;
import java.time.Duration;
import java.util.List;

public class Lab02Test {
    WebDriver driver;

    @BeforeMethod
    public void init() {
        driver = new ChromeDriver();
        driver.get("http://gatling.io");
    }

    @Test(description = "Opens Pricing page and counts the tiles")
    public void openGatlingPage() throws InterruptedException {
//        Thread.sleep(2000);
        // look for "logo_link" class, link_text = "https://gatling.io/"
        driver.findElement(By.linkText("Pricing")).click();
        // Thread.sleep because we don't know how to wait for the element yet =)
//        Thread.sleep(2000);

        // FluentWait is the thing.
        WebElement text = driver.findElement(By.xpath("//*[contains(text(), 'Cloud plans')]"));
        FluentWait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(7)).withMessage("text about Cloud Plans didn't appear");
        wait.until(ExpectedConditions.visibilityOf(text));

        // Search for the nav tabs on the page
        List<WebElement> tabs = driver.findElements(By.xpath("//*[@class='nav-tabs']/li"));

        Assert.assertEquals(tabs.size(), 4, "Didn't get 4 tabs");
        for (WebElement element : tabs) {
            Assert.assertTrue(element.isDisplayed(), "At least one of the found tabs is not displayed");
        }

        // Search for the price tabs on the page
        String[] typesOfPrices = {"Scout", "Scale", "Corporate"};
        // class can have 'contains()' too.
        List<WebElement> pricingTabs = driver.findElements(By.xpath("//div[@class='owl-stage']/div[@class='owl-item active']"));
        List<WebElement> tabNames = driver.findElements(By.xpath("//div[@class='owl-stage']/div[@class='owl-item active']//h3"));
        Assert.assertEquals(pricingTabs.size(), 3, "Not enough of those pricing tabs");

        for (int counter = 0; counter < pricingTabs.size(); counter++) {
            WebElement element = pricingTabs.get(counter);
            Assert.assertTrue(element.isDisplayed(), "At least one of the price tabs is not displayed");
            Assert.assertEquals(tabNames.get(counter).getText(), Array.get(typesOfPrices, counter));
        }

        //Search for the text
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(text(), 'Cloud plans')]")).isDisplayed(), "Could not find the 'Cloud Plans' text");
        Assert.assertEquals(driver.findElement(By.xpath("//*[contains(text(), 'Cloud plans')]")).getText(), "Cloud plans", "The text in H3 doesn't match expected");

        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(text(), 'Discover cloud load')]")).isDisplayed(), "Could not find the promo text");
        Assert.assertEquals(driver.findElement(By.xpath("//*[contains(text(), 'Discover cloud load')]")).getText(), "Discover cloud load testing with 5 minutes of test free of charge.", "The paragraph text is not what we expected");
    }

    @AfterMethod
    public void quit() {
        driver.quit();
    }
}
