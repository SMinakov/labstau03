Feature: check tariff card
  Scenario: check tariff card parameters
    Given start telekom page is open
    When navigate to tariff cards page
    And select "second" tariff card
    Then subheader is displayed
    And checkbox is not checked