Feature: Practice test task
  Scenario: check price on a smart wearable
    Given start telekom page is open
    When navigate to "MobilFunk" menu
    And check that url is "https://www.telekom.de/unterwegs"
    And check there are 6 square icons
    Then select "Geräte" icon
    And check that url is "https://www.telekom.de/unterwegs/smartphones-und-tablets"
    And check there are 5 round icons
    Then select "Smartwatches" round icon
    Then select "TCL Safety Watch MT43AX"
    And check the price
